FROM node:20-alpine3.17 as dev

LABEL org.opencontainers.image.authors="Sergey SH <sshoff@sshoff.com>"

ENV NODE_ENV development

WORKDIR /app

COPY --chown=node:node package*.json ./

RUN npm ci

COPY --chown=node:node . .

RUN npm run build

USER node


FROM node:20-alpine3.17 as prod

ENV NODE_ENV production

WORKDIR /app

COPY --chown=node:node --from=dev /app/node_modules ./node_modules

COPY --chown=node:node --from=dev /app/dist ./dist

# COPY --chown=node:node . .

COPY --chown=node:node package*.json ./

RUN npm ci --only=production && npm cache clean --force

USER node

CMD node dist/app.js