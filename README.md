# Instagram Token

Get short- and long-lived tokens via Instagram Basic Display API.

## How to run in dev mode

1. Create at the root directery of the project file `.env` and fill it with:
```
NODE_ENV="development"

PORT=4060 # If change it then change the value of `NGROK_PORT` in /docker/.env

# Don't change the DATABASE section or you will need to edit values in /docker/postgresql.env
DATABASE_BASE="host" # possible values: ["host", "aws", "gcp"]
# DATABASE_GCP_SQL_CONNECTION_NAME='' # Only for GCP
DATABASE_HOST="127.0.0.1"
DATABASE_PORT=4061
DATABASE_USER="developer"
DATABASE_PASSWORD="zaq12wsx"
DATABASE_DB="instagram_token_app"

SENTRY_DSN="<Insert Sentry DSN. At this moment this is obligatory.>"

INSTAGEAM_APP_ID="<Insert your value>"
INSTAGEAM_APP_SECRET="<Insert your value>"
INSTAGEAM_APP_REDIRECT_URI="https://<qwe-asd-zxc.ngrok-free.app>/authorize/" # Update to your value

INSTAGRAM_TOKEN_SUCCESS_REDIRECT_LINK="<A redirect link in case of success>"
INSTAGRAM_TOKEN_FAIL_REDIRECT_LINK="<A redirect link in case of fail>"

```

2. Go to `docker` directory:

```bash
cd docker
```

3. Create a file with enviroment variables for ngrok in docker compose (`.env`) from `env.example`. Fill in the fields `NGROK_AUTHTOKEN` and `NGROK_DOMAIN` with your credentials.

4. Run containers:
```bash
docker-compose up -d
```

5. Install dependencies

```bash
npm install
```

6. Run the app:
```bash
npm run start:dev
```

## How to work

### Authorize

This method gets short- and long-lived user access tokens for Instagram and saves them to DB with `requestUuid` as id.

#### Request

GET request must contain in query params:
1. A `code` from Instagram.

2. A `state` with URL-encoded JSON object with two values:

    1. `requestUuid` is a unique UUID for every request.

    2. `userUuid` is the UUID of a user in your system. For tracing purposes.

##### Example of a `state`

```json
{
    "requestUuid": "89e29e47-985c-4e72-89c6-22170ffdd9eb",
    "userUuid": "0f098520-517e-4da5-a50f-21b165508662"
}
```

##### Examples of a query to Instagram (GET)

```
https://api.instagram.com/oauth/authorize?client_id=<appId>&redirect_uri=https://qwe-asd-zxc.ngrok-free.app/authorize/&scope=user_profile,user_media&response_type=code&state=%7B%0A%20%20%20%20%22requestUuid%22%3A%20%2289e29e47-985c-4e72-89c6-22170ffdd9eb%22%2C%0A%20%20%20%20%22userUuid%22%3A%20%220f098520-517e-4da5-a50f-21b165508662%22%0A%7D
```

##### Examples of a finaly query with an URL-encoded state

```
/authorize/?code=<code>A&state=%7B%20++++%22requestUuid%22:+%2289e29e47-985c-4e72-89c6-22170ffdd9eb%22,%20++++%22userUuid%22:+%220f098520-517e-4da5-a50f-21b165508662%22%20%7D#_&e=<somthing_from_instagram>
```

or

```
/authorize/?code=<code>&state={++++"requestUuid":+"89e29e47-985c-4e72-89c6-22170ffdd9eb",++++"userUuid":+"0f098520-517e-4da5-a50f-21b165508662"}#_&e=<somthing_from_instagram>
```

##### Example of a successful response (HTTP 200)

```json
{
    "status":"ok",
    "result":"ok"
}
```

### Access Token

This method returns Instagram's User ID, short- and long-lived user access tokens from DB by `requestUuid`.

**⚠️ Attention #1!** For every `requestUuid` this returns data **only once**!

**⚠️ Attention #2!** In return value `longLivedTokenExpiration` is UTC+0 (Z) time zone!

#### Request

GET request must contain in query params `requestUuid`.

##### Example of a query

```
/access-token/?requestUuid=89e29e47-985c-4e72-89c6-22170ffdd9eb
```

##### Example of a successful response (HTTP 200)
```json
{
  "status": "ok",
  "result": {
    "userId": "00000000000000000",
    "shortLivedToken": "IGQWRNLXd5dXBXdWxvcjdKWWZASN3lrRVJQaVZAiMjA0Sk1lM0JIVFpVZADVVQUU5NEIwRVlPb1pmcUxmTlJ1MWhiamxPb1NkQ3ZA1WElPcXdaSTAtbjhabmpwRExQVi1ELW83VHZAZAX3JUVEE2SXlZAbWlaU1BwcUNLOHpCZA18xNjZASeDlPZAwZDZD",
    "longLivedToken": "IGQWRQT3JEZAmRXZAEw0Rlp4YkRHYzFTMzQ2dUlvajU0aEstczZAxZAVh5aWJBMFY3WWdOTDhVVTBRV2FnRjkzZAFN1ZAzBlUzZAHbThpNDhmTVpFeGYwUG5lX1NOdEMK74LxsQ4sSSbsNveYinPpxP4gLy",
    "longLivedTokenExpiration": "2023-12-20T05:36:09.904Z"
  }
}
```