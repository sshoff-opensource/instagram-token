import { sentryInit, sentryErrorHandler } from './common/sentry';
import express from 'express';
import config from './config/config';
import { successResponse } from './common/libs';
import { instagramAccessTokenController, instagramAuthorizeController } from './instagram/instagram.controller';

const app = express();
const port = config.app.port;

sentryInit(app);

app.get('/', (req, res) => {
    successResponse(res, 'ok');
});

app.get('/authorize', instagramAuthorizeController);

app.get('/access-token', instagramAccessTokenController);

sentryErrorHandler(app);

app.listen(port, () => {
    return console.log(`Express is listening at http://127.0.0.1:${port}`);
});