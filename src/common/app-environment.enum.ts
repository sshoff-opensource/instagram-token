export enum EAppEnvironment {
    DEV = 'dev',
    STAGE = 'stage',
    PROD = 'prod'
}