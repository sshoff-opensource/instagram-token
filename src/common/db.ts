import { Pool } from 'pg';
import config from '../config/config';

export default class Db {
    public tableName = "requests";

    private client;

    public constructor() {
        this.client = new Pool(config.db());
        this.client.connect();

        this.initDb();
    }

    public async query(query, params = []): Promise<Array<any>> {
        try {
            const answer = await this.client.query(query, params);

            return answer.rows;
        } catch (err) {
            throw new Error(err);
        }
    }

    private async initDb() {
        const checkResult = await this.checkIfDbExist();

        if (!checkResult) {
            const queryInitDB = `
                CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

                CREATE TABLE "${this.tableName}" (
                    "uuid" uuid not null default uuid_generate_v4(),
                    "requestUuid" uuid,
                    "userUuid" uuid,
                    "instagramUserId" character varying,
                    "instagramShortLivedUserAccessToken" character varying,
                    "instagramLongLivedUserAccessToken" character varying,
                    "instagramLongLivedUserAccessTokenExpiration" timestamp without time zone,
                    "returned" boolean not null default false,
                    "createdAt" timestamp not null default now(),
                    CONSTRAINT "PK_${this.tableName}" PRIMARY KEY ("uuid")
                );

                CREATE INDEX "IDX_${this.tableName}_uuid" ON "${this.tableName}" ("uuid");
                CREATE INDEX "IDX_${this.tableName}_requestUuid" ON "${this.tableName}" ("requestUuid");
                CREATE INDEX "IDX_${this.tableName}_userUuid" ON "${this.tableName}" ("userUuid");
                CREATE INDEX "IDX_${this.tableName}_createdAt" ON "${this.tableName}" ("createdAt");
            `;


            try {
                await this.client.query(queryInitDB);
            } catch (err) {
                throw new Error(err);
            }
        }
    }

    private async checkIfDbExist() {
        const query = `
            SELECT 
                COUNT(table_name)
            FROM 
                information_schema.tables 
            WHERE 
                table_schema LIKE 'public' AND 
                table_type LIKE 'BASE TABLE' AND
                table_name = '${this.tableName}';
        `;

        try {
            const res = await this.client.query(query);

            if (res.rows[0].count == 1) {
                return true;
            } else {
                return false;
            }
        } catch (err) {
            throw new Error(err);
        }
    }
}
