export function successResponse(res, result, statusCode = 200) {
    const response = {
        status: 'ok',
        result
    };

    res.setHeader('Content-Type', 'application/json');
    res.status(statusCode).json(response);
}

export function errorResponse(res, statusCode, msg) {
    const err = {
        status: 'error'
    };

    err['message'] = msg;

    res.setHeader('Content-Type', 'application/json');
    res.status(statusCode).json(err);
}