import * as dotenv from 'dotenv';
import { EAppEnvironment } from '../common/app-environment.enum';

dotenv.config();

const config = {
    app: {
        env: process.env.NODE_ENV || 'dev',
        port: parseInt(process.env.PORT) || 4060
    },

    db: function () {
        const config: { [k: string]: string | number | boolean | object } = {
            host: process.env.DATABASE_HOST,
            port: +process.env.DATABASE_PORT,
            user: process.env.DATABASE_USER,
            password: '' + process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_DB,
        };

        const dbBase = process.env.DATABASE_BASE;

        if (dbBase === 'aws') {
            config.ssl = (process.env.NODE_ENV == 'development') ? false : { rejectUnauthorized: false };
        } else if (dbBase === 'gcp') {
            config.host = (this.app.env === EAppEnvironment.PROD || this.app.env === EAppEnvironment.STAGE) ? '/cloudsql/' + process.env.DATABASE_GCP_SQL_CONNECTION_NAME : process.env.DATABASE_HOST;
        }

        return config;
    },

    sentry: {
        dsn: process.env.SENTRY_DSN
    },

    instagram: {
        clientId: process.env.INSTAGEAM_APP_ID,
        clientSecret: process.env.INSTAGEAM_APP_SECRET,
        redirectUri: process.env.INSTAGEAM_APP_REDIRECT_URI
    },

    redirects: {
        success: process.env.INSTAGRAM_TOKEN_SUCCESS_REDIRECT_LINK,
        fail: process.env.INSTAGRAM_TOKEN_FAIL_REDIRECT_LINK
    }
};

export default config;