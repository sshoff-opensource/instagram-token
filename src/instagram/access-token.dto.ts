
export class AccessTokenDto {
    protected userId: number;

    protected shortLivedToken: string;

    protected longLivedToken: string;

    protected longLivedTokenExpiration: Date;

    public constructor(requestEntity) {
        this.userId = requestEntity.instagramUserId;

        this.shortLivedToken = requestEntity.instagramShortLivedUserAccessToken;

        this.longLivedToken = requestEntity.instagramLongLivedUserAccessToken;

        this.longLivedTokenExpiration = requestEntity.instagramLongLivedUserAccessTokenExpiration;
    }
}