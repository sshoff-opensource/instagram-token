import config from '../config/config';
import { accessTokenValidation, authorizeValidation } from './instagram.validation';
import Db from '../common/db';
import { InstagramService } from './instagram.service';
import { successResponse, errorResponse } from '../common/libs';
import { AccessTokenDto } from './access-token.dto';


const db = new Db();
const igs = new InstagramService();

export async function instagramAuthorizeController(req, res) {
    if (!req.query.code){
        return errorResponse(res, 400, 'The query parameter `code` is obligatory.');
    }

    if (!req.query.state){
        return errorResponse(res, 400, 'The query parameter `state` is obligatory.');
    }

    const code = req.query.code;
    const state = JSON.parse(req.query.state.toString());

    const validation = authorizeValidation(res, {
        code,
        state
    });

    if (validation === true) {
        try {
            const result = await igs.authorize(code, state, db);

            if (result === true) {
                if (config.redirects.success) {
                    res.redirect(config.redirects.success);
                } else {
                    successResponse(res, 'ok', 200);
                }
            } else {
                if (config.redirects.fail) {
                    res.redirect(config.redirects.fail);
                } else {
                    errorResponse(res, 400, result);
                }
            }
        } catch (error) {
            errorResponse(res, 400, error.message);
        }
    }
}

export async function instagramAccessTokenController(req, res) {
    if (!req.query.requestUuid){
        return errorResponse(res, 400, 'The query parameter `requestUuid` is obligatory.');
    }

    const requestUuid = req.query.requestUuid;

    const validation = accessTokenValidation(res, {
        requestUuid
    });

    if (validation === true) {
        const result = await igs.accessToken(requestUuid, db);

        if (result.status === 'ok') {
            const accessTokenDto = new AccessTokenDto(result.result);

            successResponse(res, accessTokenDto, 200);
        } else {
            errorResponse(res, 400, result.result);
        }
    }
}