import https from 'https';
import { URL } from 'url';
import querystring from 'querystring';
import config from '../config/config';
import Db from '../common/db';

export class InstagramService {
    private urlShortLivedToken = 'https://api.instagram.com/oauth/access_token';
    private urlLongLivedToken = 'https://graph.instagram.com/access_token';

    constructor(
        private readonly clientId = config.instagram.clientId,
        private readonly clientSecret = config.instagram.clientSecret,
        private readonly redirectUri = config.instagram.redirectUri
    ) {
    }

    public async authorize(code, state, db: Db) {
        const queryCheckRequestUuid = `
            select count("uuid")
            from "${db.tableName}"
            where "requestUuid" = $1
        `;

        const rows = await db.query(queryCheckRequestUuid, [state.requestUuid]);

        if (parseInt(rows[0].count, 10) > 0) {
            return `The request with UUID ${state.requestUuid} was sent early.`;
        }

        const dataShortLivedToken = {
            'client_id': this.clientId,
            'client_secret': this.clientSecret,
            'grant_type': 'authorization_code',
            'redirect_uri': this.redirectUri,
            'code': code
        };

        const resultShortLivedToken = await this.request(this.urlShortLivedToken, 'POST', dataShortLivedToken);

        if (resultShortLivedToken) {

            const resultLongLivedToken = await this.request(`${this.urlLongLivedToken}?grant_type=ig_exchange_token&client_secret=${this.clientSecret}&access_token=${resultShortLivedToken['access_token']}`);

            if (resultLongLivedToken) {

                const experationDatetime = new Date(new Date().getTime() + resultLongLivedToken['expires_in'] * 1000);

                const query = `
                    insert into "${db.tableName}"
                    ( "requestUuid",
                        "userUuid",
                        "instagramUserId",
                        "instagramShortLivedUserAccessToken",
                        "instagramLongLivedUserAccessToken",
                        "instagramLongLivedUserAccessTokenExpiration"
                    )
                    values
                    ( $1, $2, $3, $4, $5, $6)
                `;

                const params = [
                    state.requestUuid,
                    state.userUuid,
                    resultShortLivedToken['user_id'],
                    resultShortLivedToken['access_token'],
                    resultLongLivedToken['access_token'],
                    experationDatetime
                ];

                db.query(query, params);
            }
        }

        return true;
    }

    public async accessToken(requestUuid, db: Db) {
        const query = `
            select *
            from "${db.tableName}"
            where "requestUuid" = $1
                and "returned" = false
        `;

        const rows = await db.query(query, [requestUuid]);

        if (rows.length === 0) {
            return {
                status: 'error',
                result: `Request with UUID ${requestUuid} doesn't exist or was already returned.`
            };
        }

        const queryUpdate = `
            update "${db.tableName}"
            set "returned" = true
            where "requestUuid" = $1
        `;

        db.query(queryUpdate, [requestUuid]);

        return {
            status: 'ok',
            result: rows[0]
        };
    }

    private request(url: string, method = 'GET', bodyObject = {}) {
        const uri = new URL(url);

        const body = querystring.stringify(bodyObject);

        const postOptions = {
            host: uri.hostname,
            path: uri.pathname + uri.search,
            method,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(body)
            }
        };

        return new Promise((resolve, reject) => {
            const postRequest = https.request(postOptions, (response) => {
                let result = '';

                response.setEncoding('utf8');

                response.on('error', (error) => {
                    return reject(new Error(`${response.statusCode} ${response.statusMessage} : ${error.message}`));
                });

                response.on('data', (chunk) => {
                    result += chunk;
                });

                response.on('end', () => {
                    try {
                        result = JSON.parse(result);
                    } catch (e) {
                        reject(e);
                    }

                    resolve(result);
                });
            });

            postRequest.on('error', (e) => {
                reject(e.message);
            });

            postRequest.write(body);

            postRequest.end();

        });
    }
}