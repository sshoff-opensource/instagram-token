import { errorResponse } from '../common/libs';
import validator from 'validator';

export function authorizeValidation(res, data) {
    if (!data.code) {
        return errorResponse(res, 400, 'Param `code` must be transferred.')
    }

    if (!data.state) {
        return errorResponse(res, 400, 'Param `state` must be transferred.')
    }

    if (!data.state.requestUuid || !validator.isUUID(data.state.requestUuid)) {
        return errorResponse(res, 400, 'Param `state.requestUuid` must be UUIDv4.')
    }

    if (!data.state.userUuid || !validator.isUUID(data.state.userUuid)) {
        return errorResponse(res, 400, 'Param `state.userUuid` must be UUIDv4.')
    }

    return true;
}

export function accessTokenValidation(res, data) {
    if (!data.requestUuid || !validator.isUUID(data.requestUuid)) {
        return errorResponse(res, 400, 'Param `requestUuid` must be UUIDv4.')
    }

    return true;
}